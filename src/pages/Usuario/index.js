import React, { useEffect, useState } from "react";
import { withRouter } from "react-router-dom";
import api from "../../services/api";
import GenericTable from "../../components/GenericTable";

const Usuario = () => {
  const columns = [
    {
      Header: "Nome",
      accessor: "nome", // accessor is the "key" in the data
    },
    {
      Header: "Id",
      accessor: "id_usuario",
    },
  ];

  const texts = {
    title: "Usuários",
    buttonText: "Novo usuário",
  };

  const fields = [
    {
      type: "text",
      label: "Nome",
      value: "nome",
    },
  ];

  const config = {
    service: "usuario",
    columns: columns,
    fields: fields,
    texts: texts,
  };

  return <GenericTable config={config} />;
};

export default withRouter(Usuario);
