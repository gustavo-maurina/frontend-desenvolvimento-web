import styled from "styled-components";
import theme from "../../styles/theme";

export const FormWrapper = styled.div``;

export const ContainerForm = styled.div`
	display: flex;
	flex-direction: column;
	align-items: center;
	justify-content: center;
	width: 100vw;
	overflow: hidden;

	p {
		text-align: center;
		color: ${theme.mainGreen};
	}

	input.formField,
	select {
		border-radius: 50px;
		flex: 1;
		height: 46px;
		margin-bottom: 15px;
		margin-top: 5px;
		padding: 0;
		color: #9d9d9d;
		background-color: #202020 !important;
		padding-left: 20px;
		font-size: 15px;
		width: 100%;
		border-style: solid;
		border-color: #8080805e;
		border-width: 1px;
		&::placeholder {
			color: #999;
		}
	}

	input#submitButton {
		cursor: pointer;
		color: #fff;
		font-size: 18px;
		margin-top: 20px;
		background: ${theme.mainGreen};
		height: 30px;
		border: 0;
		border-radius: 40px;
		width: 100%;
	}

	input#submitButton:disabled {
		cursor: not-allowed;
		color: #ffffff96;
		background: #06ab4e73;
	}

	.whiteText {
		cursor: pointer;
		color: white;
		margin-bottom: 30px;
	}
`;

export const ModalWrapper = styled.div`
	/* .Overlay {
		background-color: rebeccapurple;
	} */
`;
