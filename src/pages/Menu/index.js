import React, { Component } from "react";
import { Link, withRouter } from "react-router-dom";
import { Form, Container, MenuOption, AppTitle } from "./styles";

import api from "../../services/api";

class Menu extends Component {
	state = {
		username: "",
		password: "",
		error: "",
	};

	render() {
		return (
			<Container>
				<AppTitle>TaskManager</AppTitle>
				<MenuOption>
					<Link style={{ textDecoration: "none" }} to="/prioridade">
						<p className="whiteText">Prioridade</p>
					</Link>
				</MenuOption>
				<MenuOption>
					<Link style={{ textDecoration: "none" }} to="/usuario">
						<p className="whiteText">Usuários</p>
					</Link>
				</MenuOption>
				<MenuOption>
					<Link style={{ textDecoration: "none" }} to="/comentarios">
						<p className="whiteText">Comentários</p>
					</Link>
				</MenuOption>
				<MenuOption>
					<Link style={{ textDecoration: "none" }} to="/grupos">
						<p className="whiteText">Grupos</p>
					</Link>
				</MenuOption>
				<MenuOption>
					<Link style={{ textDecoration: "none" }} to="/tarefa">
						<p className="whiteText">Tarefa</p>
					</Link>
				</MenuOption>
				<MenuOption>
					<Link style={{ textDecoration: "none" }} to="/projetos">
						<p className="whiteText">Projetos</p>
					</Link>
				</MenuOption>
				<MenuOption>
					<Link style={{ textDecoration: "none" }} to="/status-tarefa">
						<p className="whiteText">Status da tarefa</p>
					</Link>
				</MenuOption>
				<MenuOption>
					<Link style={{ textDecoration: "none" }} to="/tipo-tarefa">
						<p className="whiteText">Tipo de tarefa</p>
					</Link>
				</MenuOption>
				<MenuOption>
					<Link style={{ textDecoration: "none" }} to="/sistema">
						<p className="whiteText">Sistema</p>
					</Link>
				</MenuOption>
				<MenuOption>
					<Link style={{ textDecoration: "none" }} to="/projeto-usuario">
						<p className="whiteText">Projeto &#60;&#62; Usuário</p>
					</Link>
				</MenuOption>
			</Container>
		);
	}
}
export default withRouter(Menu);
