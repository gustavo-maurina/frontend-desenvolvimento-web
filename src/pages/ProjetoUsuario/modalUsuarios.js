import React, { useEffect } from "react";
import Modal from "react-modal";
import { usePagination, useTable } from "react-table";
import api from "../../services/api";
import { TableWrapper } from "../../components/styles";
import Swal from "sweetalert2";
import { ModalWrapper } from "./styles";
import withReactContent from "sweetalert2-react-content";

const swal = withReactContent(Swal);

Modal.setAppElement("#root");

function Table({ columns, data }) {
	// Use the state and functions returned from useTable to build your UI
	const {
		getTableProps,
		getTableBodyProps,
		headerGroups,
		page,
		prepareRow,
		canPreviousPage,
		canNextPage,
		pageOptions,
		pageCount,
		gotoPage,
		nextPage,
		previousPage,
		setPageSize,
		state: { pageIndex, pageSize },
	} = useTable(
		{
			columns,
			data,
			initialState: { pageIndex: 0 },
		},
		usePagination
	);

	// Render the UI for your table
	return (
		<>
			<table {...getTableProps()}>
				<thead>
					{headerGroups.map((headerGroup) => (
						<tr {...headerGroup.getHeaderGroupProps()}>
							{headerGroup.headers.map((column) => (
								<th {...column.getHeaderProps()}>{column.render("Header")}</th>
							))}
						</tr>
					))}
				</thead>
				<tbody {...getTableBodyProps()}>
					{page.map((row, i) => {
						prepareRow(row);
						return (
							<tr {...row.getRowProps()}>
								{row.cells.map((cell) => {
									return (
										<td {...cell.getCellProps()}>{cell.render("Cell")}</td>
									);
								})}
							</tr>
						);
					})}
				</tbody>
			</table>
			{/* 
        Pagination can be built however you'd like. 
        This is just a very basic UI implementation:
      */}
			<div className="pagination">
				<div></div>
				<div>
					<div className="arrowWrapper">
						<button
							className={"button is-white fa fa-arrow-left"}
							onClick={() => previousPage()}
							disabled={!canPreviousPage}
						></button>{" "}
					</div>
					<div className="arrowWrapper">
						<button
							className={"button is-white fa fa-arrow-right"}
							onClick={() => nextPage()}
							disabled={!canNextPage}
						></button>{" "}
					</div>
				</div>
				<div>
					<span className="paginationText">
						Página{" "}
						<strong>
							{pageIndex + 1} de {pageOptions.length}
						</strong>{" "}
					</span>
				</div>
			</div>
		</>
	);
}

const ColumnsConfig = [
	{
		Header: "Usuário",
		accessor: "nome", // accessor is the "key" in the data
	},
];

const customStyles = {
	content: {
		display: "flex",
		alignItems: "center",
		justifyContent: "center",
		textAlign: "center",
		width: "40%",
		height: "60%",
		top: "50%",
		left: "50%",
		right: "auto",
		bottom: "auto",
		marginRight: "-50%",
		transform: "translate(-50%, -50%)",
		background: "#2b2b2b",
	},
	overlay: {
		backgroundColor: "rgba(255, 255, 255, 0.75)",
	},
};
const ModalUsuarios = (props) => {
	const [modalIsOpen, setIsOpen] = React.useState(false);
	const [data, setData] = React.useState(props.usuarios);

	useEffect(() => {
		console.log(props.usuarios);
		setIsOpen(props.isOpen);
		setData(props.usuarios);
		console.log(data);
	}, [props.isOpen, props.usuarios, data]);

	const closeModal = () => {
		setIsOpen(false);
		props.onClose();
	};

	const confirmDelete = async (value) => {
		await api.delete(`projeto_usuario?id=${value}`).then((r) => {
			swal.fire({
				title: "Sucesso!",
				icon: "success",
				timer: 3000,
				toast: true,
				position: "top-end",
				showConfirmButton: false,
			});
		});
	};

	const MakeColumns = () => {
		let cols = ColumnsConfig;
		let id = "id";

		cols = cols.filter((item) => item.Header != "Id");
		cols.push({
			Header: "Ações",
			id: "actions",
			accessor: "pu_id",
			Cell: ({ value }) => (
				<div>
					<button
						className={"iconButton button is-white fa fa-trash delete"}
						onClick={(e) => confirmDelete(value)}
					></button>
				</div>
			),
		});

		return cols;
	};

	const columns = React.useMemo(() => MakeColumns(), []);

	return (
		<ModalWrapper>
			{props.usuarios.length ? (
				<div>
					<Modal
						isOpen={modalIsOpen}
						//onAfterOpen={afterOpenModal}
						onRequestClose={closeModal}
						style={customStyles}
						contentLabel="Example Modal"
						// overlayClassName="Overlay"
						shouldCloseOnOverlayClick={true}
					>
						<TableWrapper>
							<Table columns={columns} data={data} />
						</TableWrapper>
					</Modal>
				</div>
			) : (
				<Modal
					isOpen={modalIsOpen}
					//onAfterOpen={afterOpenModal}
					onRequestClose={closeModal}
					style={customStyles}
					contentLabel="Example Modal"
					overlayClassName="Overlay"
					shouldCloseOnOverlayClick={true}
				>
					<p style={{ color: "white" }}>SEM USUÁRIOS NESTE PROJETO</p>
				</Modal>
			)}
		</ModalWrapper>
	);
};

export default ModalUsuarios;
