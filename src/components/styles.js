import styled from "styled-components";
import theme from "../styles/theme";

export const Container = styled.div`
	opacity: ${(props) => (props.confirmOpened ? "0.3" : "1")};
	filter: ${(props) => (props.confirmOpened ? "blur(10px)" : "blur(0px)")};
	display: flex;
	flex-direction: column;
	align-items: center;
	justify-content: center;
	height: 100vh;
	width: 100vw;
	overflow: hidden;

	.loadingText {
		margin: auto;
		color: grey;
		margin-top: 25%;
		width: 15%;
		font-size: 25px;
	}

	#voltar {
		cursor: pointer;
		color: #fff;
		font-size: 18px;
		margin-top: 20px;
		background: ${theme.mainGreen};
		height: 30px;
		border: 0;
		border-radius: 40px;
		width: 7%;
	}

	#newItemButton {
		cursor: pointer;
		background: ${theme.mainGreen};
		color: white;
		font-size: 14px;
		font-weight: bolder;
		border: none;
		border-radius: 25px;
		width: 150px;
		height: 30px;
	}
`;

export const ConfirmWrapper = styled.div`
	display: grid;
	grid-template-rows: 1fr 1fr 1fr;
	align-items: center;
	justify-content: center;
	background: unset;
	background-color: #171717;
	width: 300px;
	height: 150px;

	h2,
	p {
		color: #888484;
	}
	div {
		text-align: center;
	}

	.buttonsWrapper {
		display: flex;
		justify-content: center;
		align-items: center;

		button {
			background-color: #888484;
			color: black;
			border-style: solid;
			/* border-color: #04934275; */
			border-width: 1px;
			width: 70px;
			height: 20px;
			margin: 10px;
		}
	}
`;

export const TableWrapper = styled.div`
	width: ${(props) => (props.styles ? props.styles.width : "40vw")};
	height: 50vh;
	padding: 1rem;
	table {
		width: 100%;
		box-shadow: 3px 6px 5px 1px #0908084d;
		border-spacing: 0;
		border: 1px solid #04934275;
		border-radius: 5px;

		tr {
			:last-child {
				td {
					border-bottom: 0;
				}
			}
		}

		thead tr {
			background-color: ${theme.bgColor} !important;
		}

		th,
		td {
			font-size: 20px;
			color: grey;
			margin: 0;
			padding: 0.5rem;
			border-bottom: 1px solid #04934275;
			border-right: 1px solid #04934275;
			border-left: none;
			border-right: none;
			text-align: center;

			:last-child {
				border-right: 0;
			}
		}

		th {
			font-size: 24px;
		}
	}

	.iconButton {
		border: none;
		background-color: #80808000;
		font-size: 20px;
		cursor: pointer;
	}

	button.edit {
		color: #b5b5b5;
		padding-right: 8px;
	}

	button.delete {
		color: #f72929;
	}

	button.user {
		color: #1b7db6;
	}

	.pagination {
		margin-top: 15px;
		display: grid;
		grid-template-columns: 1fr 1fr 1fr;
		justify-content: center;
		text-align: center;

		button {
			cursor: pointer;
			border: none;
			background-color: ${theme.bgColor};
			color: ${theme.mainGreen};
			font-size: 35px;
		}

		.arrowWrapper {
			padding-left: 5px;
			padding-right: 5px;
		}

		div {
			display: inline;
		}

		.paginationText {
			font-size: 18px;
			color: grey;
			font-weight: bolder;
			line-height: 4vh;
		}
	}

	tr:nth-child(odd) {
		background-color: #242424;
	}
`;
