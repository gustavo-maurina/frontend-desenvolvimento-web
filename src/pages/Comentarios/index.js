import React, { useEffect, useState } from "react";
import { withRouter } from "react-router-dom";
import api from "../../services/api";
import GenericTable from "../../components/GenericTable";

const Comentarios = () => {
	const [tarefas, setTarefas] = useState([]);
	const [usuario, setUsuario] = useState([]);

	useEffect(async () => {
		if (!tarefas || tarefas.length == 0) {
			const result = await getTarefas();
			setTarefas(result);
		}
		if (!usuario || usuario.length == 0) {
			const result = await getUsuario();
			setUsuario(result);
		}
	}, [tarefas, usuario]);

	const getUsuario = async () => {
		let data = [];
		await api.get("usuario").then((r) => {
			r.data.map((t) => {
				data.push({ id: t.id_usuario, value: t.nome });
			});
		});
		return data;
	};

	const getTarefas = async () => {
		let data = [];
		await api.get("tarefa").then((r) => {
			r.data.map((t) => {
				data.push({ id: t.id, value: t.tarefa });
			});
		});
		return data;
	};

	const columns = [
		{
			Header: "Descrição",
			accessor: "descricao", // accessor is the "key" in the data
		},
		{
			Header: "Criado em",
			accessor: "criado", // accessor is the "key" in the data
		},
		{
			Header: "Editado em",
			accessor: "editado", // accessor is the "key" in the data
		},
		{
			Header: "Nome",
			accessor: "nome", // accessor is the "key" in the data
		},
		{
			Header: "Tarefa",
			accessor: "tarefa", // accessor is the "key" in the data
		},
		{
			Header: "Id",
			accessor: "id",
		},
	];

	const texts = {
		title: "Comentários",
		buttonText: "Novo comentário",
	};

	const fields = [
		{
			type: "text",
			label: "Descrição",
			value: "descricao",
		},
		{
			type: "select",
			label: "Tarefa",
			value: "id_tarefa",
			options: tarefas,
		},
		{
			type: "select",
			label: "Usuário",
			value: "id_usuario",
			options: usuario,
		},
	];

	const config = {
		service: "comentario",
		columns: columns,
		fields: fields,
		texts: texts,
		styles: {
			width: "70vw",
		},
	};

	return <GenericTable config={config} />;
};

export default withRouter(Comentarios);
