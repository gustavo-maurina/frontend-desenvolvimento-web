import { createGlobalStyle } from "styled-components";

import "font-awesome/css/font-awesome.css";
import theme from "./theme";

const GlobalStyle = createGlobalStyle`
* {
	box-sizing: border-box;
	padding: 0;
	margin: 0;
	outline: 0;
}
body, html {
	overflow-x: hidden;
  overflow-y: hidden;
	background: #2b2b2b;
	font-family: 'Helveteica Neue', 'Helvetica', Arial;
	height: 100%;
	width: 100%;
}

p {
	font-size: 18px;
	font-weight: bold;
}

.whiteText {
	color: white;
}

.rdt_Table {
	background-color: ${theme.bgColor};
}

.react-confirm-alert-overlay {
	background: unset !important;
}
`;

export default GlobalStyle;
