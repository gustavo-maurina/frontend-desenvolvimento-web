import styled from "styled-components";
import theme from "./theme";

export const AppTitle = styled.p`
	font-size: 104px;
	margin-bottom: 15px;
	color: ${theme.mainGreen};
`;
