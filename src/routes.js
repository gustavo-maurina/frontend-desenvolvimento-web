import React from "react";
import { BrowserRouter, Route, Switch, Redirect } from "react-router-dom";
//npm install react-router-dom axios styled-components prop-types font-awesome

import { isAuthenticated } from "./services/auth";
import Registrar from "./pages/Registrar";
import Login from "./pages/Login";
import Menu from "./pages/Menu";
import Usuario from "./pages/Usuario";
import Tarefa from "./pages/Tarefa";
import Prioridades from "./pages/Prioridades";
import CreateTarefa from "./pages/Tarefa/tarefa";
import EditaTarefa from "./pages/Tarefa/editaTarefa";
import TipoTarefa from "./pages/TipoTarefa";
import StatusTarefa from "./pages/StatusTarefa";
import Comentarios from "./pages/Comentarios";
import Grupos from "./pages/Grupos";
import Projetos from "./pages/Projetos";
import Sistema from "./pages/Sistema";
import ProjetoUsuario from "./pages/ProjetoUsuario";

const PrivateRoute = ({ component: Component, ...rest }) => (
	<Route
		{...rest}
		render={(props) =>
			isAuthenticated() ? (
				<Component {...props} />
			) : (
				<Redirect to={{ pathname: "/", state: { from: props.location } }} />
			)
		}
	/>
);

const Routes = () => (
	<BrowserRouter>
		<Switch>
			<Route exact path="/" component={Login} />
			<Route path="/registrar" component={Registrar} />
			<PrivateRoute path="/menu" component={Menu} />
			<PrivateRoute path="/projetos" component={Projetos} />
			<PrivateRoute path="/tarefa" component={Tarefa} />
			<PrivateRoute path="/grupos" component={Grupos} />
			<PrivateRoute path="/tipo-tarefa" component={TipoTarefa} />
			<PrivateRoute path="/prioridade" component={Prioridades} />
			<PrivateRoute path="/comentarios" component={Comentarios} />
			<PrivateRoute path="/createTarefa" component={CreateTarefa} />
			<PrivateRoute path="/projeto-usuario" component={ProjetoUsuario} />
			<PrivateRoute path="/editaTarefa/:id" component={EditaTarefa} />
			<PrivateRoute path="/status-tarefa" component={StatusTarefa} />
			<PrivateRoute path="/usuario" component={Usuario} />
			<PrivateRoute path="/sistema" component={Sistema} />
			<Route path="*" component={() => <h3>404 não encontrado</h3>} />
		</Switch>
	</BrowserRouter>
);

export default Routes;
