import React, { useEffect, useState } from "react";
import { withRouter } from "react-router-dom";
import api from "../../services/api";
import GenericTable from "../../components/GenericTable";

const Prioridades = () => {
	const columns = [
		{
			Header: "Descrição",
			accessor: "descricao", // accessor is the "key" in the data
		},
		{
			Header: "Id",
			accessor: "id",
		},
	];

	const texts = {
		title: "Prioridade",
		buttonText: "Nova prioridade",
	};

	const fields = [
		{
			type: "text",
			label: "Descrição",
			value: "descricao",
		},
	];

	const config = {
		service: "prioridade",
		columns: columns,
		fields: fields,
		texts: texts,
	};

	return <GenericTable config={config} />;
};

export default withRouter(Prioridades);
