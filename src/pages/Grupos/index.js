import React, { useEffect, useState } from "react";
import { withRouter } from "react-router-dom";
import api from "../../services/api";
import GenericTable from "../../components/GenericTable";

const Grupos = () => {
	const [projetos, setProjetos] = useState([]);

	useEffect(async () => {
		if (!projetos || projetos.length == 0) {
			const result = await getProjetos();
			setProjetos(result);
		}
	}, [projetos]);

	const getProjetos = async () => {
		let data = [];
		await api.get("projeto").then((r) => {
			r.data.map((t) => {
				data.push({ id: t.id, value: t.projeto });
			});
		});
		return data;
	};

	const columns = [
		{
			Header: "Descrição",
			accessor: "descricao", // accessor is the "key" in the data
		},
		{
			Header: "Id",
			accessor: "id",
		},
	];

	const texts = {
		title: "Grupos",
		buttonText: "Novo grupo",
	};

	const fields = [
		{
			type: "text",
			label: "Descrição",
			value: "descricao",
		},
		{
			type: "select",
			label: "Projeto",
			value: "id_projeto",
			options: projetos,
		},
	];

	const config = {
		service: "grupo",
		columns: columns,
		fields: fields,
		texts: texts,
	};

	return <GenericTable config={config} />;
};

export default withRouter(Grupos);
