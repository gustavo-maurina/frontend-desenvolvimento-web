import styled from "styled-components";
import theme from "../../styles/theme";

export const FormWrapper = styled.div`
	display: grid;
	grid-template-columns: repeat(auto-fit, 15vw);
	grid-gap: 10px;
	width: 70vw;
`;

export const Container = styled.div`
	display: flex;
	flex-direction: column;
	align-items: center;
	justify-content: center;
	text-align: center;
	width: 100vw;
	height: 100vh;

	p {
		text-align: center;
		color: ${theme.mainGreen};
	}

	input.formField,
	select,
	button.formField {
		border-radius: 50px;
		flex: 1;
		height: 46px;
		margin-bottom: 15px;
		margin-top: 5px;
		padding: 0;
		color: #9d9d9d;
		background-color: #202020 !important;
		padding-left: 20px;
		font-size: 15px;
		width: 100%;
		border-style: solid;
		border-color: #8080805e;
		border-width: 1px;
		&::placeholder {
			color: #999;
		}
	}

	button.formField {
		width: 280px;
		text-align: left;
	}

	input[type="number"] {
		-webkit-appearance: none;
		margin: 0;
	}

	input::-webkit-outer-spin-button,
	input::-webkit-inner-spin-button {
		display: none;
	}

	input#submitButton {
		cursor: pointer;
		color: #fff;
		font-size: 18px;
		margin-top: 20px;
		background: ${theme.mainGreen};
		height: 40px;
		border: 0;
		border-radius: 40px;
		width: 7%;
	}

	input#submitButton:disabled {
		cursor: not-allowed;
		color: #ffffff96;
		background: #06ab4e73;
	}
`;
