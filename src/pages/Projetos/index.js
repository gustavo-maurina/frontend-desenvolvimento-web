import React, { useEffect, useState } from "react";
import { withRouter } from "react-router-dom";
import api from "../../services/api";
import GenericTable from "../../components/GenericTable";

const Projetos = () => {
	const [sistema, setSistema] = useState([]);

	useEffect(() => {
		const fetchSistema = async () => {
			await getSistema().then((s) => {
				setSistema(s);
			});
		};
		fetchSistema();
	}, []);

	const getSistema = async () => {
		let data = [];

		await api.get("/sistema").then((r) => {
			let response = r.data;
			response.map((r) => {
				return data.push({ id: r.id_sistema, value: r.nome });
			});
		});

		return data;
	};

	const columns = [
		{
			Header: "Título",
			accessor: "projeto", // accessor is the "key" in the data
		},
		{
			Header: "Descrição",
			accessor: "descricao", // accessor is the "key" in the data
		},
		{
			Header: "Id",
			accessor: "id",
		},
		{
			Header: "Sistema",
			accessor: "sistema", // accessor is the "key" in the data
		},
	];

	const texts = {
		title: "Projeto",
		buttonText: "Novo projeto",
	};

	const fields = [
		{
			type: "text",
			label: "Descrição",
			value: "descricao",
		},
		{
			type: "text",
			label: "Título",
			value: "titulo",
		},
		{
			type: "select",
			label: "Sistema",
			value: "id_sistema",
			options: sistema,
		},
	];

	const config = {
		service: "projeto",
		columns: columns,
		fields: fields,
		texts: texts,
		styles: {
			width: "60vw",
		},
	};

	return (
		<>
			{sistema.length > 0 ? (
				<GenericTable config={config} />
			) : (
				<div>carregando...</div>
			)}
		</>
	);
};

export default withRouter(Projetos);
